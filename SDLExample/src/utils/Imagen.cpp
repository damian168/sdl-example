/*
 * Imagen.cpp
 *
 *  Created on: Aug 16, 2013
 *      Author: damian
 */

#include <utils/Imagen.h>

Imagen::Imagen(SDL_Surface *screen, const TipoImagen *tipo_imagen,
		unsigned int x, unsigned int y) {
	_screen = screen;
	_tipo_imagen = tipo_imagen;
	_x = x;
	_y = y;
	_rectangle.x = _x;
	_rectangle.y = _y;
	_offset.x = _x;
	_offset.y = _y;
	_offset.h = tipo_imagen->h();
	_offset.w = tipo_imagen->w();
	_currentSprite = 0;
	_blitCounter = 0;
}

void Imagen::blit(int rate) {
	_blitCounter++;
	if (_blitCounter % rate == 0) {
		if (_tipo_imagen->sprites() > 1) {
			_currentSprite = _currentSprite % _tipo_imagen->sprites();
			_offset.x = _rectangle.w * _currentSprite;
			_currentSprite++;
		}
		_blitCounter = 0;
	}
	SDL_BlitSurface((SDL_Surface *) _tipo_imagen->image(), &_offset, _screen,
			&_rectangle);
}

unsigned int Imagen::x(void) const {
	return _x;
}

unsigned int Imagen::y(void) const {
	return _y;
}

unsigned int Imagen::x(unsigned int x) {
	_x = x;
	_rectangle.x = _x;
	return _x;
}

unsigned int Imagen::y(unsigned int y) {
	_y = y;
	_rectangle.y = _y;
	return _y;
}

