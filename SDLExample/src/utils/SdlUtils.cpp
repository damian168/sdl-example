/*
 * SdlUtils.cpp
 *
 *  Created on: Aug 15, 2013
 *      Author: damian
 */

#include <SDL/SDL.h>
#include <string>
#include "SdlUtils.h"

SdlUtils::SdlUtils() {
	// TODO Auto-generated constructor stub

}

SdlUtils::~SdlUtils() {
	// TODO Auto-generated destructor stub
}

SDL_Surface* SdlUtils::load_image(std::string filename) {
	SDL_Surface* loadedImage = NULL;
	SDL_Surface* optimizedImage = NULL;
	loadedImage = SDL_LoadBMP( filename.c_str() );
	if( loadedImage != NULL )
	{
		optimizedImage = SDL_DisplayFormat( loadedImage );
		SDL_FreeSurface( loadedImage );
	}
	return optimizedImage;
}

void SdlUtils::apply_surface(int x, int y, SDL_Surface* source, SDL_Surface* destination) {
	SDL_Rect offset;
	offset.x = x;
	offset.y = y;
	SDL_BlitSurface( source, NULL, destination, &offset );
}
