/*
 * TipoImagen.h
 *
 *  Created on: Aug 16, 2013
 *      Author: damian
 */

#ifndef TIPOIMAGEN_H_
#define TIPOIMAGEN_H_
#include <SDL/SDL.h>

class TipoImagen
{
  private:
    const char *_filepath;
    SDL_Surface *_loaded_image;
    SDL_Surface *_display_image;

    unsigned int _sprites;
    Uint16 _w;
    Uint16 _h;

  public:
    TipoImagen(const char *filepath, int sprites = 1);
    ~TipoImagen(void);
    const SDL_Surface *image(void) const;
    const unsigned int sprites() const;
    const Uint16 w() const;
    const Uint16 h() const;
};

#endif /* TIPOIMAGEN_H_ */
