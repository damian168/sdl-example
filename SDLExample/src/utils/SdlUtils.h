/*
 * SdlUtils.h
 *
 *  Created on: Aug 15, 2013
 *      Author: damian
 */

#ifndef SDLUTILS_H_
#define SDLUTILS_H_
#include <SDL/SDL.h>
#include <iosfwd>

class SdlUtils {
public:
	SdlUtils();
	virtual ~SdlUtils();
	static SDL_Surface *load_image( std::string filename );
	static void apply_surface(int x, int y, SDL_Surface* source, SDL_Surface* destination);
};

#endif /* SDLUTILS_H_ */
