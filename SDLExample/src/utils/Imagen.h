/*
 * Imagen.h
 *
 *  Created on: Aug 16, 2013
 *      Author: damian
 */

#ifndef IMAGEN_H_
#define IMAGEN_H_
#include "TipoImagen.h"
#include <SDL/SDL.h>
#include <iostream>

using namespace std;

class Imagen
{
  private:

    SDL_Surface *_screen;
    SDL_Rect _rectangle;
    SDL_Rect _offset;
    const TipoImagen *_tipo_imagen;
    unsigned int _x;
    unsigned int _y;
    unsigned int _currentSprite;
    unsigned int _blitCounter;

  public:

    Imagen(SDL_Surface *screen = NULL, const TipoImagen *tipo_imagen = NULL, unsigned int x = 0, unsigned int y = 0);
    void blit(int rate = 1);
    unsigned int x(void) const;
    unsigned int y(void) const;
    unsigned int x(unsigned int x);
    unsigned int y(unsigned int y);
};


#endif /* IMAGEN_H_ */
