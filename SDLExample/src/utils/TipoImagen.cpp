/*
 * TipoImagen.cpp
 *
 *  Created on: Aug 16, 2013
 *      Author: damian
 */
#include "TipoImagen.h"
#include <SDL/SDL_image.h>
#include <SDL/SDL.h>

TipoImagen::TipoImagen(const char *filepath, int sprites) {
	_sprites = sprites;
	_filepath = strdup(filepath);
	_loaded_image = IMG_Load(_filepath);
	if (!_loaded_image) {
		throw "Image was not successfully loaded.";
	}
	_display_image = SDL_DisplayFormat(_loaded_image);
	_w = _display_image->w / sprites;
	_h = _display_image->h;

	if (_display_image != NULL) {
		// transparencia para un color
		Uint32 colorkey = SDL_MapRGB(_display_image->format, 0xFF, 0xFF, 0xFF);
		SDL_SetColorKey(_display_image, SDL_SRCCOLORKEY, colorkey);
	} else {
		throw "Image was not successfully converted to display format.";
	}
}

TipoImagen::~TipoImagen(void) {
	if (_loaded_image)
		SDL_FreeSurface(_loaded_image);
	if (_display_image)
		SDL_FreeSurface(_display_image);
}

const SDL_Surface* TipoImagen::image(void) const {
	return _display_image;
}

const unsigned int TipoImagen::sprites() const {
	return _sprites;
}

const Uint16 TipoImagen::w() const {
	return _w;
}

const Uint16 TipoImagen::h() const {
	return _h;
}
