/*
 * Juego.cpp
 *
 *  Created on: Aug 19, 2013
 *      Author: damian
 */

#include <SDL/SDL.h>
#include <controllers/Juego.h>

Juego::Juego() {
	screen = NULL;
	holaT = NULL;
	backT = NULL;
	hola = NULL;
	back = NULL;
}

Juego::~Juego() {
}

SDL_Surface* Juego::initSDL(int screenWidth, int screenHeight, int screenColorDepth) {
	if (SDL_Init(SDL_INIT_EVERYTHING) == -1) {
			return NULL;
	}
	SDL_Surface *screen = SDL_SetVideoMode(screenWidth, screenHeight,
				screenColorDepth, SDL_SWSURFACE | SDL_DOUBLEBUF);
	SDL_EnableKeyRepeat(100, SDL_DEFAULT_REPEAT_INTERVAL);
	SDL_WM_SetCaption("Hello World", NULL);

	return screen;
}

bool Juego::handleKeyPress(SDL_keysym keysym) {
	SDLKey key = keysym.sym;
	int holaX = hola->x();
	int holaY = hola->y();
	switch (key) {
	case SDLK_LEFT:
		hola->x(--holaX);
		break;
	case SDLK_RIGHT:
		hola->x(++holaX);
		break;
	case SDLK_UP:
		hola->y(--holaY);
		break;
	case SDLK_DOWN:
		hola->y(++holaY);
		break;
	case SDLK_q:
	case SDLK_ESCAPE:
		return true;
		break;
	default:
		break;
	}
	return false;
}


void Juego::eventLoop() {
	bool quit = false;
	SDL_Event event;

	while (!quit) {
		while (SDL_PollEvent(&event)) {
			back->blit();
			hola->blit(2);
			SDL_Flip(screen);

			switch (event.type) {
			case SDL_KEYDOWN:
				quit = handleKeyPress(event.key.keysym);
				break;
			case SDL_QUIT:
				quit = true;
				break;
			default:
				break;
			}
		}
	}

}

int Juego::start(JuegoConfig* config) {
	screen = initSDL(800, 600, 32); // todo: get from config
	if (screen == NULL) {
		return 1;
	}

	holaT = new TipoImagen("resources/sonic.png", 6);
	backT = new TipoImagen("resources/back.jpg");
	hola = new Imagen(screen, holaT, 0, 0);
	back = new Imagen(screen, backT, 0, 0);

	eventLoop();

	//Quit SDL
	delete holaT;
	delete backT;
	delete hola;
	delete back;
	SDL_Quit();
	return EXIT_SUCCESS;
}
