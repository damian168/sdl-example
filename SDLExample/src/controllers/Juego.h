/*
 * Juego.h
 *
 *  Created on: Aug 19, 2013
 *      Author: damian
 */

#ifndef JUEGO_H_
#define JUEGO_H_
#include <controllers/JuegoConfig.h>
#include <SDL/SDL.h>
#include <utils/TipoImagen.h>
#include <utils/Imagen.h>

class Juego {
	SDL_Surface* screen;
	TipoImagen* holaT;
	TipoImagen* backT;
	Imagen* hola;
	Imagen* back;

	SDL_Surface* initSDL(int screenWidth, int screenHeight, int screenColorDepth);
	void eventLoop();
	bool handleKeyPress(SDL_keysym keysym);
public:
	Juego();
	virtual ~Juego();
	int start(JuegoConfig*);
};

#endif /* JUEGO_H_ */
