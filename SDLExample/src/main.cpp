#include <SDL/SDL_events.h>
#include <controllers/Juego.h>
#include <controllers/JuegoConfig.h>

int main(void) {
	JuegoConfig* config = new JuegoConfig();
	Juego* juego = new Juego();
	return juego->start(config);
}
